const MT940Validator = require('./modules/mt940-validator');

const path = process.argv.slice(2)[0] || 'data/records.csv';

const validator = new MT940Validator()

validator.validate(path, true);
validator.validate('data/records-changed.csv', true );
