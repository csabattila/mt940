const csv = require('csvtojson');
const iban = require('iban');
const Big = require('big.js'); //javascript calculations issues
const fs = require('fs');

module.exports = class MT940Validator {
    issueFilePath;

    constructor(issuesFilePath = 'issues-found.txt') {
        this.issueFilePath = issuesFilePath;

        try {
            fs.writeFileSync(this.issueFilePath, `Validation results: \n`, {
                flag: "w"
            });
        } catch (err) {
            console.error(err);
        }
    }

    validate(path, output = false) {
        console.log(`Validation in progress for file '${path}' ...`);

        csv()
            .fromFile(path)
            .then((data) => this._validation(data, path, output),
                (error) => console.log(`Could not load or parse csv file due to ${error}`));
    }

    _validation = (data, path, output) => {
        const issueHelper = {path: path, issues: [], uniqueReferences: []}

        data.forEach((record, index) => this._validationFn(record, index, issueHelper));

        this.#saveIssues(issueHelper.issues, issueHelper.path, output);
    };

    _validationFn = (record, index, issueHelper) => {
        if (this._validateUnique(record['Reference'], issueHelper.uniqueReferences)) {
            issueHelper.uniqueReferences.push(record['Reference'].trim().toLocaleLowerCase());
        } else {
            this._pushIssue(index + 1, 'Reference number is a duplicate', issueHelper.issues);
        }

        if (!this._validateBalance(record['Start Balance'], record['Mutation'], record['End Balance'])) {
            this._pushIssue(index + 1, 'End balance is wrong', issueHelper.issues);
        }

        if (!iban.isValid(record['Account Number'])) {
            this._pushIssue(index + 1, 'Invalid iban', issueHelper.issues);
        }
    };

    // kinda trivial but just for the structure and modularity
    _validateUnique = (reference, checkAgainst) => !checkAgainst.includes(reference);

    _validateBalance = (startBalance, mutation, endBalance) => {
        // assuming that mutation always has + or - sign, i guess the format requires that.
        if (mutation.indexOf('-') === 0) {
            return new Big(startBalance).minus(new Big(mutation.substring(1))).eq(new Big(endBalance))
        } else if (mutation.indexOf('+') === 0) {
            return new Big(startBalance).plus(new Big(mutation.substring(1))).eq(new Big(endBalance))
        }
    };

    _pushIssue = (recordNumber, issue, issues) => {
        const recordWithIssue = issues.find(issue => issue.recordNumber === recordNumber);

        if (recordWithIssue) {
            recordWithIssue.issues.push(issue)
        } else {
            issues.push({
                recordNumber: recordNumber,
                issues: [issue]
            })
        }
    };

    #saveIssues = (issues, path, output) => {
        const issueHeaderText = `\nPath ${path}: ${issues.length} invalid record(s) found \n`;

        if (output) {
            console.log(issueHeaderText.replace(` \n`,''));
        }

        this.#writeIssues(issueHeaderText);

        if (issues.length) {
            issues.forEach(issue => {
                const issueTextRecord = `record: ${issue.recordNumber}, issues: ${issue.issues.join(', ')} \n`;

                if (output) {
                    console.log(issueTextRecord.replace(` \n`,''));
                }

                this.#writeIssues(issueTextRecord);
            });
        }
    };

    #writeIssues = (content) => {
        try {
            fs.writeFileSync(this.issueFilePath, content, {
                flag: "a"
            });
        } catch (err) {
            console.error(err);
        }
    };
}
