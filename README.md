Very simple MT940 validator
==========================

This is a simple MT940 data sheet validator. It checks for duplicated reference and wrong end balance.

Instalation
-----------

Node version required is v12.13.0 (assume and v12 would work)

Run 

`npm i`

in order to get all the dependencies

Usage
-----

Enter project directory.

Run 

`npm run script`

in order to validate the default data file located in the `data` folder 

Run 

`npm run script -- <path>`

to validate a file located on the `path` command line argument

Test
----

Run 

`npm run test`

in order to run the unit test

__Notes:__ 

 *   I tried using the new private methods but then could not add unit test for those. Then I reverted the `#` to `_`.
 *   Added a quick iban validator
 *   Added a `records-changed.csv` file for further validation

  
