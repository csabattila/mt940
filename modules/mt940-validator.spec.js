const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require("sinon-chai");

chai.use(sinonChai);

const MT940Validator = require('./mt940-validator.js');

describe("csvValidator", () => {
    let mT940Validator;
    let assert = chai.assert;
    let expect = chai.expect;

    beforeEach(() => {
        mT940Validator = new MT940Validator();
    });

    describe("_validateUnique", () => {
        it("should return valid duplicate check ", () => {
            assert.equal(mT940Validator._validateUnique('1', ['1']), false);
            assert.equal(mT940Validator._validateUnique('1', [1]), true);
            assert.equal(mT940Validator._validateUnique('1', ['1',1]), false);
        });
    });

    describe("_validateBalance", () => {
        it("should return valid balance check ", () => {
            assert.equal(mT940Validator._validateBalance('2', '+1', '3'), true);
            assert.equal(mT940Validator._validateBalance('2', '+2', '2'), false);
            assert.equal(mT940Validator._validateBalance('2.22', '-1.11', '1.11'), true);
        });
    });

    // a bit of integration
    describe("_validationFn", () => {
        it("should push iusses", () => {
            const spy = sinon.spy(mT940Validator, "_pushIssue");

            mT940Validator._validationFn({ Reference: '112', 'Start Balance': '1', Mutation: '+2','End Balance': '5'}, 0, {path: 'test.csv', issues:[], uniqueReferences: []});
            expect(spy).to.have.been.calledWith(1, 'End balance is wrong', [{ issues: ["End balance is wrong", "Invalid iban"], recordNumber: 1 }]);
        });
    });
});
